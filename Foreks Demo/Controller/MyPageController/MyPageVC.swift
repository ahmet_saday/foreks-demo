//
//  MyPageVC.swift
//  Foreks Demo
//
//  Created by Saday on 23.09.2020.
//  Copyright © 2020 Ahmet Saday. All rights reserved.
//

import UIKit

class MyPageVC: BaseVC {
    
    @IBOutlet weak var myPageTableView: UITableView!
    @IBOutlet weak var firstFilterButtonLabel: UIButton!
    @IBOutlet weak var secondFilterButtonLabel: UIButton!
    @IBOutlet weak var fieldPickerView: UIPickerView!
    
    @IBOutlet weak var constraintOfBottomForTableView: NSLayoutConstraint!
    
    let vm = MyPageVM()
    
    //MARK: Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup
        setup()
    }
    
    //MARK: Functions
    func setup() {
        
        constraintOfBottomForTableView.constant = 0
        
        view.backgroundColor = .black
        
        firstFilterButtonLabel.setTitle(vm.firstField.name, for: .normal)
        secondFilterButtonLabel.setTitle(vm.secondField.name, for: .normal)
        
        myPageTableView.delegate = self
        myPageTableView.dataSource = self
        myPageTableView.backgroundColor = .black
        myPageTableView.register(UINib.init(nibName: Constants.MyPage.myPageTableViewCell, bundle: nil), forCellReuseIdentifier: Constants.MyPage.myPageTableViewCell)
        
        vm.delegate = self
        vm.getMyPageTableViewCellData()
        
        fieldPickerView.delegate = self
        fieldPickerView.dataSource = self
        fieldPickerView.backgroundColor = .white
        fieldPickerView.isHidden = true
        
        Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
            self.vm.updateMyPageTableViewCellData()
        }
    }
    
    //MARK: Actions
    @IBAction func filterButtonPressed(_ sender: UIButton) {
        
        fieldPickerView.isHidden = !fieldPickerView.isHidden
        
        // UIPickerView constant settings
        if fieldPickerView.isHidden {
            constraintOfBottomForTableView.constant = 0
        } else {
            constraintOfBottomForTableView.constant = -157
        }
        
        // Column selection duo update
        if sender.tag == 1 {
            vm.columnField = ColumnField.first
        }
        if sender.tag == 2{
            vm.columnField = ColumnField.second
        }
    }
    
}

//MARK: MyPageDelegate
extension MyPageVC: MyPageDelegate {
    func reloadTableView() {
        myPageTableView.reloadData()
        fieldPickerView.reloadAllComponents()
    }
    
    func showErrorMessage(message: String) {
        self.showMessage(message: message)
    }
    
}

//MARK: UITableViewDataSource
extension MyPageVC: UITableViewDataSource {
     
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.vm.MyPageTableViewCellArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.MyPage.myPageTableViewCell, for: indexPath) as! MyPageTableViewCell
        
        let item = self.vm.MyPageTableViewCellArray[indexPath.row]
        cell.arrange(item: item, firstFieldName: self.vm.firstField.name, secondFieldName: self.vm.secondField.name)
        
        return cell
    }
}

//MARK: UITableViewDelegate
extension MyPageVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

//MARK: PickerView Delegate
extension MyPageVC: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return vm.myPageArray[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if vm.columnField == .first {
            firstFilterButtonLabel.setTitle(vm.myPageArray[row].name, for: .normal)
            vm.firstField = vm.myPageArray[row]
        }
        if vm.columnField == .second {
            secondFilterButtonLabel.setTitle(vm.myPageArray[row].name, for: .normal)
            vm.secondField = vm.myPageArray[row]
        }
        
        
        fieldPickerView.isHidden = true
        constraintOfBottomForTableView.constant = 0
    }
}

//MARK: PickerView DataSource
extension MyPageVC: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return vm.myPageArray.count
    }
    
}
