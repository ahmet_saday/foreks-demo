//
//  MyPageTableViewCell.swift
//  Foreks Demo
//
//  Created by Saday on 23.09.2020.
//  Copyright © 2020 Ahmet Saday. All rights reserved.
//

import UIKit

class MyPageTableViewCell: UITableViewCell {
    
    @IBOutlet weak var splashView: UIView!
    @IBOutlet weak var symbolNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var firstFieldLabel: UILabel!
    @IBOutlet weak var secondFieldLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        
    }
    
    func arrange(item: MyPageTableViewCellModel, firstFieldName: String, secondFieldName: String) {
        let vm = MyPageTableViewCellVM(item: item,firstField: firstFieldName,secondField: secondFieldName)
        
        symbolNameLabel.text = vm.getSymbolName()
        timeLabel.text = vm.getTimeString()
        
        let firstFieldFormat = vm.getFirstFieldFormatted()
        firstFieldLabel.text = firstFieldFormat.0
        firstFieldLabel.textColor = firstFieldFormat.1
        
        let secondFieldFormat = vm.getSecondFieldFormatted()
        secondFieldLabel.text = secondFieldFormat.0
        secondFieldLabel.textColor = secondFieldFormat.1
        
        arrowImageView.image = UIImage(named: vm.getPicture())
        
        self.splashView.alpha = 0

        mainThread {
            if item.isUpdated {
                UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .allowUserInteraction, animations: {
                    self.splashView.alpha = 0.5

                }) { (complate) in
                    self.hideAnimation()
                }
            }
        }
        
    }
    
    private func hideAnimation() {
        mainThread {
            UIView.animate(withDuration: 0.4, delay: 0, options: .allowUserInteraction, animations: {
                self.splashView.alpha = 0
            }) { (complate) in

            }
        }
    }
    
}
