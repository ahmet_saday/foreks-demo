//
//  MyPageTableViewCellVM.swift
//  Foreks Demo
//
//  Created by Saday on 27.09.2020.
//  Copyright © 2020 Ahmet Saday. All rights reserved.
//

import UIKit

class MyPageTableViewCellVM {
    
    private(set) var item: MyPageTableViewCellModel!
    private(set) var firstField: String!
    private(set) var secondField: String!

    init(item: MyPageTableViewCellModel, firstField: String, secondField: String) {
        self.item = item
        self.firstField = firstField
        self.secondField = secondField
    }

    
    func getSymbolName() -> String {
        return item.symbolName
    }
    
    func getTimeString() -> String {
        
        return item.timeString
    }
    
    func getPicture() -> String {
        return item.picture
    }
    
    func getFirstFieldFormatted() -> (String,UIColor) {
        return fieldFormatter(fieldFormat: firstField, value: item.firstField)
    }
    
    func getSecondFieldFormatted() -> (String,UIColor) {
        return fieldFormatter(fieldFormat: secondField, value: item.secondField)
    }
    
   
    
    
    
    private func fieldFormatter(fieldFormat: String, value: String) -> (String,UIColor) {
        
        let subString = "-"
        // %Fark format
        if fieldFormat == "%Fark" {
            if value.contains(subString){
                return ("% \(value)",.red)
            }
            return ("% \(value)",.green)
        }
        
        // Fark format
        if fieldFormat == "Fark" {
            if value.contains(subString){
                return (value, .red)
            }
            return (value,.green)
        }
        
        // Other formats
        return (value,.white)
    }
}
