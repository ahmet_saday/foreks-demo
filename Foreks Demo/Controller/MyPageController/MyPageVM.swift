//
//  MyPageVM.swift
//  Foreks Demo
//
//  Created by Saday on 25.09.2020.
//  Copyright © 2020 Ahmet Saday. All rights reserved.
//

import Foundation
import UIKit

protocol MyPageDelegate {
    func showErrorMessage(message: String)
    func reloadTableView()
}

class MyPageVM {
    
    var MyPageTableViewCellArray = [MyPageTableViewCellModel]()
    var pageDefaultsArray = [MypageDefault]()
    var myPageArray = [Mypage]()
    
    var firstField = Mypage(name: "Son", key: "las")
    var secondField = Mypage(name: "%Fark", key: "pdd")
    
    var columnField = ColumnField.first
    
    var delegate: MyPageDelegate!
    
    
    
    func getMyPageTableViewCellData(){
        
        NetworkService.request(router: .getMyPageSettings) { (result: MyPageDefaultModel) in
            
            self.pageDefaultsArray = result.mypageDefaults
            self.myPageArray = result.mypage
            
            self.pageDefaultsArray.forEach { pageDefault in
                let tke = pageDefault.tke
                let sybomName = pageDefault.cod
                let requestField = self.firstField.key + "," + self.secondField.key
                
                NetworkService.request(router: .getSymbolData(fields: requestField, stcs: tke)) { (symbol: SymbolModel) in
                                            
                guard symbol.l.count > 0 else {return}
                    let myPageTableViewCellItem = MyPageTableViewCellModel(picture: PictureType.minus.rawValue, symbolName: sybomName,
                                                                           timeString: symbol.l[0]["clo"] ?? "00.00.00",
                                                                           firstField: symbol.l[0][self.firstField.key] ?? "-",
                                                                           secondField: symbol.l[0][self.secondField.key] ?? "-",
                                                                           las: symbol.l[0]["las"] ?? "1", isUpdated: false)
                    self.MyPageTableViewCellArray.append(myPageTableViewCellItem)
                    self.delegate.reloadTableView()
                    
                }
                
            }
            
        }
    }
    
    func updateMyPageTableViewCellData() {
        
        pageDefaultsArray.forEach { (pageDefaultItem) in
            let cod = pageDefaultItem.cod
            let tke = pageDefaultItem.tke
            
            MyPageTableViewCellArray.enumerated().forEach { (index, myPageTableViewCellItem) in
                if myPageTableViewCellItem.symbolName == cod {
                    
                    let requestField = self.firstField.key + "," + self.secondField.key
                    NetworkService.request(router: .getSymbolData(fields: requestField, stcs: tke)) { (symbol: SymbolModel) in
                                                
                        guard symbol.l.count > 0 else {return}
                    
                        // Update row model via new values
                        let timeString = symbol.l[0]["clo"] ?? "00.00.00"
                        let oldTimeString = self.MyPageTableViewCellArray[index].timeString
                        self.MyPageTableViewCellArray[index].timeString = symbol.l[0]["clo"] ?? "00.00.00"
                        self.MyPageTableViewCellArray[index].isUpdated = (timeString == oldTimeString) ? false : true
                        self.MyPageTableViewCellArray[index].firstField = symbol.l[0][self.firstField.key] ?? "-"
                        self.MyPageTableViewCellArray[index].secondField = symbol.l[0][self.secondField.key] ?? "-"
                        self.MyPageTableViewCellArray[index].picture = self.updateArrowImageWithLasValue(newLasValue: symbol.l[0]["las"] ?? "0", oldLasValue: myPageTableViewCellItem.las)
                        self.MyPageTableViewCellArray[index].las =  String(symbol.l[0]["las"] ?? "0")

                        // update tableview
                        self.delegate.reloadTableView()
                    }
                }
            }
        }
    }
    
    func updateArrowImageWithLasValue(newLasValue: String, oldLasValue: String) -> String {
        
        let _newLasValue = Double(newLasValue.stringWithoutComma())!
        let _oldLasValue = Double(oldLasValue.stringWithoutComma())!
        
        return (_newLasValue > _oldLasValue) ? PictureType.up.rawValue : (_newLasValue < _oldLasValue) ? PictureType.down.rawValue :  PictureType.minus.rawValue
        
    }
    
    
}

enum ColumnField {
    case first
    case second
}

