//
//  GlobalHelpers.swift
//  Foreks Demo
//
//  Created by Saday on 28.09.2020.
//  Copyright © 2020 Ahmet Saday. All rights reserved.
//

import Foundation

func mainThread(main: @escaping () -> ()) {
    DispatchQueue.main.async(execute: {
        main()
    })
}
