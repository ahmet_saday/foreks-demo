//
//  Constants.swift
//  Foreks Demo
//
//  Created by Saday on 23.09.2020.
//  Copyright © 2020 Ahmet Saday. All rights reserved.
//

import Foundation

struct Constants {
    
    struct MyPage {
        static let myPageTableViewCell = "MyPageTableViewCell"
    }
}
