//
//  SayfamTableViewCellModel.swift
//  Foreks Demo
//
//  Created by Saday on 25.09.2020.
//  Copyright © 2020 Ahmet Saday. All rights reserved.
//

import Foundation

struct MyPageTableViewCellModel {
    
    var picture: PictureType.RawValue
    let symbolName: String
    var timeString: String
    var firstField: String
    var secondField: String
    var las: String
    var isUpdated: Bool

}
enum PictureType: String {
    case up = "up"
    case down = "down"
    case minus = "minus"
}
