//
//  MyPageDefaultModel.swift
//  Foreks Demo
//
//  Created by Saday on 24.09.2020.
//  Copyright © 2020 Ahmet Saday. All rights reserved.
//

import Foundation

struct MyPageDefaultModel: Codable {
    let mypageDefaults: [MypageDefault]
    let mypage: [Mypage]
}

// MARK: - Mypage
struct Mypage: Codable {
    let name, key: String
}

// MARK: - MypageDefault
struct MypageDefault: Codable {
    let cod, gro, tke, def: String
}
