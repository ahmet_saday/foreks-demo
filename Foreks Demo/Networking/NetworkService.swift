//
//  NetworkService.swift
//  Foreks Demo
//
//  Created by Saday on 23.09.2020.
//  Copyright © 2020 Ahmet Saday. All rights reserved.
//

import Foundation

class NetworkService {
    
    class func request<T: Codable>(router: Router, complation: @escaping (T) -> ()) {
        
        var components = URLComponents()
        components.scheme = router.scheme
        components.host = router.host
        components.path = router.path
        components.queryItems = router.parameters
        
        let session = URLSession(configuration: .default)
        guard let url = components.url else {return}
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = router.method
        
        let dataTask = session.dataTask(with: urlRequest) { data, response, error in
            
            guard error == nil else {
                print(error?.localizedDescription ?? "Session Data Task Error")
                return
            }
            guard response != nil else {
                print("no response")
                return
            }
            guard let data = data else {
                print("no else")
                return
            }
            
            
            let responseObject = try! JSONDecoder().decode(T.self, from: data)
            DispatchQueue.main.async {
                complation(responseObject)
//                print(responseObject)
            }
        }
        dataTask.resume()
    }
}
