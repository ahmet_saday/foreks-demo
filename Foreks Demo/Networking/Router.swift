//
//  Router.swift
//  Foreks Demo
//
//  Created by Saday on 23.09.2020.
//  Copyright © 2020 Ahmet Saday. All rights reserved.
//

import Foundation

enum Router {
    case getMyPageSettings, getSymbolData(fields: String, stcs: String)
    
    var scheme: String {
        switch self {
        case .getMyPageSettings, .getSymbolData:
            return "https"
        }
        
    }
    
    var host: String {
        switch self {
        case .getMyPageSettings, .getSymbolData:
            return "sui7963dq6.execute-api.eu-central-1.amazonaws.com"
        }
    }
    
    var path: String {
        switch  self {
        case .getMyPageSettings:
            return "/default/ForeksMobileInterviewSettings"
        case .getSymbolData:
            return "/default/ForeksMobileInterview"
        }
    }
    
    var parameters: [URLQueryItem]? {
        switch self {
        case .getSymbolData(let field, let stcs):
            return [URLQueryItem(name: "fields", value: field),
                URLQueryItem(name: "stcs", value: stcs)
            ]
        case .getMyPageSettings:
            return nil
        }
    }
    
    var method: String {
        switch self {
        case .getMyPageSettings, .getSymbolData:
            return "GET"
        }
    }
    
}
