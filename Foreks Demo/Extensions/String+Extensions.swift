//
//  String+Extension.swift
//  Foreks Demo
//
//  Created by Saday on 27.09.2020.
//  Copyright © 2020 Ahmet Saday. All rights reserved.
//

import Foundation

extension String {
    
    func stringWithoutComma() -> String {
        return self.replacingOccurrences(of: ",", with: "", options: NSString.CompareOptions.literal, range: nil)
    }
}
